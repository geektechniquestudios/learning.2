package com.example.dependencyinjectiondemo.dependencyinjection.package2;

import com.example.dependencyinjectiondemo.dependencyinjection.package1.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@ComponentScan({"com.example.dependencyinjectiondemo.dependencyinjection.package1","com.example.dependencyinjectiondemo.dependencyinjection.package2"})
@SpringBootApplication
public class DependencyInjectionApplication {

	@Bean
	public User user(){
		return new User("Terry","Dorsey", "geektechniquestudios@gmail.com") ;
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(DependencyInjectionApplication.class, args);

		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for(String name : beanNames){
			System.out.println(name);
		}

		System.out.println(ctx.getBean("user").toString());
	}

}
