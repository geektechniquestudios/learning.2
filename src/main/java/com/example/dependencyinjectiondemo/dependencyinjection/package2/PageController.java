package com.example.dependencyinjectiondemo.dependencyinjection.package2;

import com.example.dependencyinjectiondemo.dependencyinjection.package1.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PageController {

    //notification service

    private NotificationService notificationService;

    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RequestMapping("/")
    public String home(){
        return notificationService.toString();
        //return "text to return to page";
    }

//    @RequestMapping("/error") // curious why this doesn't work. Just messing with controller
//    public String errorMessage(){ // answered here : https://www.logicbig.com/tutorials/spring-framework/spring-boot/implementing-error-controller.html
//        return "something isn't working";
//    }
}
