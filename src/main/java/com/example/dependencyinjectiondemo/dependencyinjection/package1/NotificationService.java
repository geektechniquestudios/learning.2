package com.example.dependencyinjectiondemo.dependencyinjection.package1;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("testingRenamingOfService")
public class NotificationService {

    public NotificationService(){

    }

    public void send(){
        //do something
    }

    public void sendAsync(){
        //do something
    }

    @Override
    public String toString() {
        return "hello from notification service";
    }
}
